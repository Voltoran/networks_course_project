import os
import email
import sending_mail as smpt
import parse_message as pm


def menu():
    mlist = list()
    get_flag = 0

    while True:
        print("\nMenu")
        print("1. Send mail.")
        print("2. Get all new mails.")

        if get_flag == 1:
            print("3. Get current message list.")
            print("4. Get message with number.")
        index = input("> ")

        if index == "1":
            make_mail()
        elif index == "2":
            get_messages()
            temp = pm.get_mail_list()
            if temp == None:
                print("No new messages for you!")
            else:
                mlist = temp
                get_flag = 1
                print("You have " + str(len(mlist)) + " new messages!")
                k = 0
                for i in mlist:
                    print("\n****Message " + str(k + 1) + "****")
                    print("From ", i["From"])
                    print("Info ", i["Info"])
                    #print("File name", i["Content-Disposition"].split(";")[1].strip().split("=")[1].replace('"', ''))
                    k += 1
        elif index == "3" and get_flag == 1:
            k = 0
            for i in mlist:
                print("\n****Message " + str(k + 1) + "****")
                print(i["From"])
                print(i["Info"])
                k += 1
        elif index == "4" and get_flag == 1:
            number = input("Number of message: ")
            try:
                pm.open_message(mlist[int(number) - 1])
                input("Press any key to continue...")
            except IndexError:
                print("Wrong number!")
        else:
            print("Unknown command!")


def get_messages():
    os.system("python3 pop_messages.py")


def make_mail():
    _from = input("From: ")
    _to = input("To: ")
    _info = input("Info to attachment: ")
    _attach_filename = input("File name (file must be in current directory): ")

    smpt.send_mail(_from, _to, _info, _attach_filename)


menu()