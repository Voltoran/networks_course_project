import email
import base64
import os

expected_dir = "./temp.txt"
N = 18  # magic number (not)


def get_mail_list(filepath=expected_dir):
    message_list = list()
    message = ""
    try:
        with open(filepath) as file:
            lines = file.readlines()
    except FileNotFoundError:
        return None

    if len(lines) != 0 and len(lines) % 18 == 0:  # ???
        for j in range(len(lines)):
            message += lines[j]
            if (j + 1) % 18 == 0:
                message_list.append(email.message_from_string(message))
                message = ""

        os.remove(filepath)

        return message_list
    else:
        return None


def open_message(message):
    print("File info " + message["Info"])

    file = message.get_payload()[0]
    name = file["Content-Disposition"].split(";")[1].strip().split("=")[1].replace('"', '')
    text = file.get_payload()
    print("File " + name + " saved in recv_data directory!")

    data = base64.standard_b64decode(text)

    out = open("recv_data/" + name, "wb")
    out.write(data)
    out.close()