from twisted.internet import protocol, reactor
import threading as tr

host = 'localhost'
port = 6666

class Twist_client(protocol.Protocol):
    def sendData(self):
        data = input("Write login and password like <login password>: ")
        if data:
            self.transport.write(data.encode("UTF-8"))
        else:
            self.transport.loseConnection()

    def connectionMade(self):
        self.sendData()

    def dataReceived(self, data):
        f = open("temp.txt", "ab")
        f.write(data)
        f.close()


class Twist_Factory(protocol.ClientFactory):
    protocol = Twist_client

    def clientConnectionFailed(self, connector, reason):
        # print('connection failed:', reason.getErrorMessage())
        reactor.stop()

    def clientConnectionLost(self, connector, reason):
        # print('connection lost:', reason.getErrorMessage())
        reactor.stop()

factory = Twist_Factory()
reactor.connectTCP(host, port, factory)
reactor.run()
