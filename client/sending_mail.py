from email import utils
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import base64

# Create the message
def send_mail(_from, _to, _info, _attach_filename):
    msg = MIMEMultipart()
    msg['To'] = utils.formataddr(('', _to))
    msg['From'] = utils.formataddr(('', _from))
    msg['Subject'] = 'Simple pdf message'
    msg['Info'] = _info

    f = open("./" + _attach_filename, 'rb')
    data = f.read()
    f.close()

    data = base64.b64encode(data)
    str_data = ""

    for i in data:
        str_data += chr(i)
    part = MIMEText(str_data, 'pdf')
    part.add_header('Content-Disposition', 'attachment', filename=_attach_filename)
    msg.attach(part)

    from smtplib import LMTP
    client = LMTP('localhost', 8025)
    client.sendmail(_from, [_to], msg.as_bytes())

    print("Message send successfully!")
