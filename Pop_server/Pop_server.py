import shutil
import os

from twisted.internet import protocol, reactor
from twisted.protocols.policies import TimeoutMixin


users = []

def take_file_list(my_dir):
    files = list()
    try:
        onlyfiles = [f for f in os.listdir(my_dir) if os.path.isfile(os.path.join(my_dir, f))]
    except FileNotFoundError:
        return None

    for i in onlyfiles:
        files.append(my_dir + '/' + i)

    return files


class Twist(protocol.Protocol, TimeoutMixin):
    # Событие connectionMade срабатывает при соединении
    def connectionMade(self):
        pass

    def dataReceived(self, data):
        return_message = data.decode("UTF-8")
        login_pass_pair = return_message.strip()

        auth = open("users", "r")
        data = auth.read()
        users = data.split("\n")
        auth.close()

        dir = '../Smtp_server/MailFolder/' + login_pass_pair.split(" ")[0] + '/new'
        dir_old = '../Smtp_server/MailFolder/' + login_pass_pair.split(" ")[0] + '/old'

        print(dir)

        if login_pass_pair in users:
            try:
                files = take_file_list(dir)
            except FileNotFoundError or FileExistsError:
                files = None
            self.manage(files, dir_old)
        else:
            try:
                files = take_file_list(dir)
                auth = open("users", "a")
                auth.write("\n" + login_pass_pair)
                auth.close()
                users.append(login_pass_pair)

                self.manage(files, dir_old)
            except FileNotFoundError:
                pass

        self.transport.loseConnection()

    def manage(self, files, dir_old):
        if files == None or files == []:
            pass
        else:
            try:
                os.makedirs(dir_old)
            except FileExistsError:
                pass

            for i in files:
                self.sendFile(i)
                shutil.copy(i, dir_old, follow_symlinks=True)
                os.remove(i)

    def sendFile(self, filename):
        f = open(filename, "rb")
        self.transport.write(f.read())
        f.close()

    def connectionLost(self, reason):
        print("Lost connection!")

    def timeoutConnection(self):
        self.transport.write("[Disconnect] U connection closed! Try again if u want))".encode("UTF-8"))
        self.transport.abortConnection()


# Конфигурация поведения протокола описывается в – классе Factory из twisted.internet.protocol.Factory
factory = protocol.Factory()
factory.protocol = Twist

print('wait...')
reactor.listenTCP(6666, factory)
reactor.run()

